
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsCategoryMap : UrsEntityTypeConfiguration<GoodsCategory>
    {
        public override void Configure(EntityTypeBuilder<GoodsCategory> builder)
        {
            builder.ToTable(nameof(GoodsCategory));
            builder.HasKey(pc => pc.Id);
            
            base.Configure(builder);
        }
    }
}