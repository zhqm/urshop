using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Users;

namespace Urs.Data.Mapping.Users
{
    public partial class UserMap : UrsEntityTypeConfiguration<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User));
            builder.HasKey(c => c.Id);
            builder.Property(u => u.Nickname).HasMaxLength(1000);
            builder.Property(u => u.Email).HasMaxLength(1000);
            builder.Property(u => u.PhoneNumber).HasMaxLength(1000);
            builder.Property(u => u.Password);
            builder.Property(c => c.AdminComment);
        
            builder.Property(user => user.ShippingAddressId).HasColumnName("ShippingAddress_Id");
            
            builder.HasOne(c => c.ShippingAddress)
                .WithMany()
                .HasForeignKey(c => c.ShippingAddressId);

            builder.Ignore(user => user.UserRoles);
            builder.Ignore(user => user.Addresses);
            builder.Ignore(u => u.PasswordFormat);

            base.Configure(builder);
        }
    }
}