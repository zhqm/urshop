﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Users;

namespace Urs.Data.Mapping.Users
{
    public partial class MessageRecordMap : UrsEntityTypeConfiguration<MessageRecord>
    {
        public override void Configure(EntityTypeBuilder<MessageRecord> builder)
        {
            builder.ToTable(nameof(MessageRecord));
            builder.HasKey(c => c.Id);

            base.Configure(builder);
        }
    }
}
