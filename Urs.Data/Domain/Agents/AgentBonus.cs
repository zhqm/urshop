﻿using System;
using Urs.Core;

namespace Urs.Data.Domain.Agents
{
    /// <summary>
    /// 代理分销分红
    /// </summary>
    public class AgentBonus : BaseEntity
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public virtual int OrderId { get; set; }
        /// <summary>
        /// 订单总金额
        /// </summary>
        public virtual decimal OrderTotal { get; set; }
        /// <summary>
        /// 有效佣金金额
        /// </summary>
        public virtual decimal Price { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public virtual int UserId { get; set; }
        /// <summary>
        /// 代理Id
        /// </summary>
        public virtual int AgentId { get; set; }
        /// <summary>
        /// 佣金
        /// </summary>
        public virtual decimal Fee { get; set; }
        /// <summary>
        /// 是否提现
        /// </summary>
        public virtual bool Cash { get; set; }
        /// <summary>
        /// 上级代理Id
        /// </summary>
        public virtual int ParentAgentId { get; set; }
        /// <summary>
        /// 上级佣金
        /// </summary>
        public virtual decimal ParentFee { get; set; }
        /// <summary>
        /// 上级是否提现
        /// </summary>
        public virtual bool ParentCash { get; set; }
        /// <summary>
        /// 是否有效，默认有效，如有取消设置为false
        /// </summary>
        public virtual bool IsValid { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; }
    }
}
