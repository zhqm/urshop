﻿using System.Collections.Generic;
using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class ExternalAuthSettings : ISettings
    {
        public ExternalAuthSettings()
        {
            ActiveAuthenticationMethodSystemNames = new List<string>();
        }

        public bool AutoRegisterEnabled { get; set; }
        /// <summary>
        /// Gets or sets an system names of active payment methods
        /// </summary>
        public List<string> ActiveAuthenticationMethodSystemNames { get; set; }
        /// <summary>
        /// 微信小程序AppId
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        /// 微信小程序Secret
        /// </summary>
        public string AppSecret { get; set; }
    }
}