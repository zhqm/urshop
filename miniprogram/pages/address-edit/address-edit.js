var area = require('../../data/area')
var p = 0,
  c = 0,
  d = 0
import api from '../../api/api'
import {
  addressadd
} from '../../api/conf'
import {
  addressedit
} from '../../api/conf'

Page({
  data: {
    addAddress: true,
    provinceName: [],
    provinceCode: [],
    provinceSelIndex: '',
    cityName: [],
    cityCode: [],
    citySelIndex: '',
    districtName: [],
    districtCode: [],
    districtSelIndex: '',
    showMessage: false,
    messageContent: '',
    showDistpicker: false,
    addressItem: null,
    adresName: '',
    adresPhone: '',
    adresInfo: '',
    adresId: null
  },
  onLoad: function(options) {
    // 载入时要显示再隐藏一下才能显示数据，如果有解决方法可以在issue提一下，不胜感激:-)
    // 初始化数据
    this.setAreaData()
    if (options.item) {
      let item = JSON.parse(options.item);
      this.setData({
        addressItem: item,
        addAddress: false,
        adresName: item.Name,
        adresPhone: item.Phone,
        adresInfo: item.Address,
        adresId: item.Id
      })
    }
    if (this.data.addAddress) {
      wx.setNavigationBarTitle({
        title: '添加地址'
      })
    } else {
      wx.setNavigationBarTitle({
        title: '编辑地址'
      })
    }
  },
  onShow: function() {
    if (!this.data.addAddress) {
      let p = 0
      let c = 0
      let d = 0
      for (let i = 0; i < this.data.provinceName.length; i++) {
        if (this.data.addressItem.ProvinceName == this.data.provinceName[i]) {
          p = i
        }
      }
      this.setAreaData(p)
      for (let j = 0; j < this.data.cityName.length; j++) {
        if (this.data.addressItem.CityName == this.data.cityName[j]) {
          c = j
        }
      }
      this.setAreaData(p, c)
      for (let z = 0; z < this.data.districtName.length; z++) {
        if (this.data.addressItem.AreaName == this.data.districtName[z]) {
          d = z
        }
      }
      this.setAreaData(p, c, d)
      this.setData({
        provinceSelIndex: p,
        citySelIndex: c,
        districtSelIndex: d
      })
    }
  },
  deletAdres: function () {
    var that = this
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          api.post('/v1/address/delete?addressId=' + that.data.adresId, {
          }).then(res => {
            if (res.Code == 200) {
              wx.navigateBack()
            }
          })
        }
      }
    })
  },
  setAreaData: function(p, c, d) {
    var p = p || 0 // provinceSelIndex
    var c = c || 0 // citySelIndex
    var d = d || 0 // districtSelIndex
    // 设置省的数据
    var province = area['100000']
    var provinceName = [];
    var provinceCode = [];
    for (var item in province) {
      provinceName.push(province[item])
      provinceCode.push(item)
    }
    this.setData({
      provinceName: provinceName,
      provinceCode: provinceCode
    })
    // 设置市的数据
    var city = area[provinceCode[p]]
    var cityName = [];
    var cityCode = [];
    for (var item in city) {
      cityName.push(city[item])
      cityCode.push(item)
    }
    this.setData({
      cityName: cityName,
      cityCode: cityCode
    })
    // 设置区的数据
    var district = area[cityCode[c]]
    var districtName = [];
    var districtCode = [];
    for (var item in district) {
      districtName.push(district[item])
      districtCode.push(item)
    }
    this.setData({
      districtName: districtName,
      districtCode: districtCode
    })
  },
  changeArea: function(e) {
    p = e.detail.value[0]
    c = e.detail.value[1]
    d = e.detail.value[2]
    this.setAreaData(p, c, d)
  },
  showDistpicker: function() {
    this.setData({
      showDistpicker: true
    })
  },
  distpickerCancel: function() {
    this.setData({
      showDistpicker: false
    })
  },
  distpickerSure: function() {
    this.setData({
      provinceSelIndex: p,
      citySelIndex: c,
      districtSelIndex: d
    })
    this.distpickerCancel()
  },
  savePersonInfo: function(e) {
    var that = this
    var data = e.detail.value
    var telRule = /^1[3|4|5|7|8]\d{9}$/,
      nameRule = /^[\u2E80-\u9FFF]+$/
    if (data.adresName == '') {
      this.showMessage('请输入姓名')
    } else if (data.adresPhone == '') {
      this.showMessage('请输入手机号码')
    } else if (!telRule.test(data.adresPhone)) {
      this.showMessage('手机号码格式不正确')
    } else if (data.province == '') {
      this.showMessage('请选择所在地区')
    } else if (data.city == '') {
      this.showMessage('请选择所在地区')
    } else if (data.district == '') {
      this.showMessage('请选择所在地区')
    } else if (data.adresInfo == '') {
      this.showMessage('请输入详细地址')
    } else if (!this.data.addAddress) {
      api.post(addressedit + '?addressId=' + this.data.addressItem.Id, {
        Name: data.adresName,
        Phone: data.adresPhone,
        ProvinceName: that.data.provinceName[that.data.provinceSelIndex],
        CityName: that.data.cityName[that.data.citySelIndex],
        AreaName: that.data.districtName[that.data.districtSelIndex],
        Address: data.adresInfo
      }).then((res) => {
        if (res.Code == 200) {
          that.showMessage('保存成功')
          wx.navigateBack()
        }
      })
    } else {
      api.post(addressadd, {
        Name: data.adresName,
        Phone: data.adresPhone,
        ProvinceName: that.data.provinceName[that.data.provinceSelIndex],
        CityName: that.data.cityName[that.data.citySelIndex],
        AreaName: that.data.districtName[that.data.districtSelIndex],
        Address: data.adresInfo
      }).then((res) => {
        if (res.Code == 200) {
          that.showMessage('添加成功')
          wx.navigateBack()
        }
      })
    }
  },
  showMessage: function(text) {
    var that = this
    that.setData({
      showMessage: true,
      messageContent: text
    })
    setTimeout(function() {
      that.setData({
        showMessage: false,
        messageContent: ''
      })
    }, 3000)
  },
  inputName: function(e) {
    this.setData({
      adresName: e.detail.value
    })
  },
  inputPhone: function(e) {
    this.setData({
      adresPhone: e.detail.value
    })
  },
  inputAdres: function(e) {
    this.setData({
      adresInfo: e.detail.value
    })
  }
})