import api from '../../api/api'
import { register } from '../../api/conf'
import { sendsmsforregister } from '../../api/conf'
Page({
  data: {
    yzmtxt: '发送验证码',
    yzmTime: 60,
    phone: '',
    guid: ''
  },
  onLoad: function (options) {
    
  },
  sendyzm:function(){
    if (this.data.phone == ''){
      wx.showToast({
        title: '手机号码不能为空',
        icon: 'none',
        duration: 2000 
      })
      return
    }
    if (this.data.yzmTime != 60){
      return
    }
    var that = this
    api.post(sendsmsforregister + '?phone=' + that.data.phone,{
    }).then((res)=>{
      var timer = setInterval(() => {
        if (that.data.yzmTime == 1) {
          clearInterval(timer)
          that.setData({
            yzmtxt: '发送验证码',
            yzmTime: 60
          })
          return
        }
        that.data.yzmTime--;
        that.setData({
          yzmtxt: '倒计时' + that.data.yzmTime + '秒'
        })
      }, 1000)
    }).catch((err)=>{
      wx.showToast({
        title: err.Content,
        icon: 'none',
      })
    })
  },
  formSubmit(e) {
    if (e.detail.value.pwd != e.detail.value.pwd2){
      wx.showToast({
        title: '密码不一致',
        icon: 'none',
      })
      return
    } else if (e.detail.value.phone == ''){
      wx.showToast({
        title: '手机号不能为空',
        icon: 'none',
      })
      return
    } else if (e.detail.value.yzm == ''){
      wx.showToast({
        title: '请输入验证码',
        icon: 'none',
      })
      return
    }
    var that = this
    api.post(register,{
      Phone: e.detail.value.phone,
      Token: e.detail.value.yzm,
      Password: e.detail.value.pwd2,
      InviteCode: e.detail.value.yqm
    }).then((res)=>{
      wx.showToast({
        title: '注册成功',
      })
      that.formReset()
    }).catch((err)=>{
      wx.showToast({
        title: '验证码错误',
        icon: 'none',
      })
    })
  },
  formReset() {
    setTimeout(()=>{
      wx.navigateTo({
        url: '/pages/login/login',
      })
    },1600)
  },
  getphone: function(e){
    this.setData({
      phone: e.detail.value
    })
  }
})