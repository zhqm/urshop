﻿using FluentValidation;
using Urs.Admin.Models.Stores;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Urs.Admin.Validators.Stores
{
    public class CategoryValidator : BaseUrsValidator<CategoryModel>
    {
        public CategoryValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotNull().WithMessage(localizationService.GetResource("Admin.Store.Categories.Fields.Name.Required"));
        }
    }
}