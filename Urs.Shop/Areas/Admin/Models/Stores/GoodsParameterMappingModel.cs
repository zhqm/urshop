﻿using Microsoft.AspNetCore.Mvc;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Stores
{
    public partial class GoodsParameterMappingModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Store.Goods.GoodsParameters.Fields.GoodsParameter")]
        
        public string GoodsParameterName { get; set; }

        [UrsDisplayName("Admin.Store.Goods.GoodsParameters.Fields.GoodsParameterOption")]
        
        public string GoodsParameterOptionName { get; set; }

        [UrsDisplayName("Admin.Store.Goods.GoodsParameters.Fields.CustomValue")]
        
        public string CustomValue { get; set; }

        [UrsDisplayName("Admin.Store.Goods.GoodsParameters.Fields.AllowFiltering")]
        public bool AllowFiltering { get; set; }

        [UrsDisplayName("Admin.Store.Goods.GoodsParameters.Fields.ShowOnGoodsPage")]
        public bool ShowOnGoodsPage { get; set; }

        [UrsDisplayName("Admin.Store.Goods.GoodsParameters.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}