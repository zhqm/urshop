﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Urs.Framework;
using Urs.Framework.Kendoui;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Coupons
{
    public class CouponModel : BaseEntityModel
    {
        /// <summary>
        /// 优惠券价值
        /// </summary>
       [DisplayName("优惠面值")]
        [Required]
        public  decimal Value { get; set; }
        /// <summary>
        /// 唯一码
        /// </summary>
        //[DisplayName("优惠券码")]
        //[Required]
        //public  string UniqueCode { get; set; }
        /// <summary>
        /// 优惠券的开始时间
        /// </summary>
        [DisplayName("开始时间")]
        [UIHint("DateNullable")]
        [Required]
        public  DateTime StartTime { get; set; }
        /// <summary>
        /// 截止时间
        /// </summary>
        [DisplayName("截止时间")]
        [UIHint("DateNullable")]
        [Required]
        public  DateTime EndTime { get; set; }
        /// <summary>
        /// 最低消费金额
        /// </summary>
         [DisplayName("最低消费金额")]
         [Required]
        public  decimal MinimumConsumption { get; set; }
         //<summary>
         //数量
         //</summary>
        [DisplayName("数量")]
        [Required]
        public  int Amount { get; set; }
        [DisplayName("优惠券主题")]
        [Required]
        public string Title { get; set; }
        [DisplayName("优惠券链接")]
        public string Url { get; set; }

        [DisplayName("剩余数量")]
        public int UsedAmount { get; set; }
        [DisplayName("数量限制？")]
        public  bool IsAmountLimit { get; set; }
        /// <summary>
        /// 获取时间
        /// </summary>
        public string GotTime { get; set; }
        /// <summary>
        /// 是否已经使用了
        /// </summary>
        public string IsUsed { get; set; }
        /// <summary>
        /// 使用时间
        /// </summary>
        public string UsedTime { get; set; }
        [DisplayName("备注")]
        public string Remark { get; set; }

        public partial class AddUserBindingModel : BaseModel
        {
            public AddUserBindingModel()
            {
                SearchUserRoleIds = new List<int>();
                AvailableUserRoles = new List<SelectListItem>();
            }
            public ResponseResult Users { get; set; }

            [UrsDisplayName("Admin.Users.Users.List.UserRoles")]
            
            public List<SelectListItem> AvailableUserRoles { get; set; }
            [UrsDisplayName("Admin.Users.Users.List.UserRoles")]
            [UIHint("MultiSelect")]
            public IList<int> SearchUserRoleIds { get; set; }

            [UrsDisplayName("Admin.Users.Users.List.SearchEmail")]
            
            public string SearchEmail { get; set; }

            [UrsDisplayName("Admin.Users.Users.List.SearchUsername")]
            
            public string SearchUsername { get; set; }
            public bool UsernamesEnabled { get; set; }

            [UrsDisplayName("Admin.Users.Users.List.SearchPhone")]
            
            public string SearchPhone { get; set; }
            public int CouponId { get; set; }
            public int[] SelectedUserIds { get; set; }

            public int UserId { get; set; }
        }
    }
}