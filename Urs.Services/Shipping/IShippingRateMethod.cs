using Microsoft.AspNetCore.Routing;
using Urs.Core.Plugins;

namespace Urs.Services.Shipping
{
    public partial interface IShippingRateMethod : IPlugin
    {
        ShippingRateMethodType ShippingRateMethodType { get; }

        GetShippingOptionResponse GetShippingOptions(GetShippingOptionRequest getShippingOptionRequest);

        decimal? GetFixedRate(GetShippingOptionRequest getShippingOptionRequest);
    }
}
