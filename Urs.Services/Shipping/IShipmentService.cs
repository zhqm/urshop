using System;
using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Shipping;

namespace Urs.Services.Shipping
{
    public partial interface IShipmentService
    {
        void DeleteShipment(Shipment shipment);

        IPagedList<Shipment> GetAllShipments(string expressName, string trackingNumber, DateTime? createdFrom = null, DateTime? createdTo = null,
            int pageIndex = 0, int pageSize = int.MaxValue);

        IList<Shipment> GetNotDeliverShipments(DateTime? startTime, DateTime? endTime);
        IList<Shipment> GetShipmentsByIds(int[] shipmentIds);

        Shipment GetShipmentById(int shipmentId);

        Shipment GetShipmentByOrderId(int orderGoodsId);

        void InsertShipment(Shipment shipment);

        void UpdateShipment(Shipment shipment);



        void DeleteShipmentOrderGoods(ShipmentOrderItem sopv);

        ShipmentOrderItem GetShipmentOrderGoodsById(int sopvId);

        void InsertShipmentOrderGoods(ShipmentOrderItem sopv);

        void UpdateShipmentOrderGoods(ShipmentOrderItem sopv);
    }
}
