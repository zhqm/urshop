using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Stores;

namespace Urs.Services.Stores
{
    public partial interface ICategoryService
    {
        void DeleteCategory(Category category);

        IPagedList<Category> GetAllCategories(string categoryName = "",
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        IList<Category> GetAllCategoriesByParentCategoryId(int parentCategoryId, bool showHidden = false);

        IList<Category> GetAllCategoriesDisplayedOnHomePage();

        Category GetCategoryById(int categoryId);

        void InsertCategory(Category category);

        void UpdateCategory(Category category);

        void DeleteGoodsCategory(GoodsCategory goodsCategory);

        IPagedList<GoodsCategory> GetGoodsCategoriesByCategoryId(int categoryId,
            int pageIndex, int pageSize, bool showHidden = false);

        IList<Goods> GetFeaturedGoodss(int categoryId, bool showHidden = false);
        GoodsCategory GetGoodsCategoryById(int goodsCategoryId);

        IList<GoodsCategory> GetGoodsCategoriesByGoodsId(int goodsId);

        void InsertGoodsCategory(GoodsCategory goodsCategory);

        void UpdateGoodsCategory(GoodsCategory goodsCategory);

        string GetFormattedBreadCrumb(Category category, IList<Category> allCategories = null, string separator = ">>");
        
        IList<Category> GetCategoriesByGoodsId(int goodsId);

        IList<Category> GetCategoryBreadCrumb(Category category, IList<Category> allCategories = null, bool showHidden = false);
    }
}
