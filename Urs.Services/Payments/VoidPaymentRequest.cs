﻿using Urs.Data.Domain.Orders;

namespace Urs.Services.Payments
{
    public partial class VoidPaymentRequest
    {
        public Order Order { get; set; }
    }
}
