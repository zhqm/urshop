﻿using System.Collections.Generic;
using Urs.Data.Domain.Payments;

namespace Urs.Services.Payments
{
    public partial class CapturePaymentResult
    {
        private PaymentStatus _newPaymentStatus = PaymentStatus.Pending;
        public IList<string> Errors { get; set; }

        public CapturePaymentResult() 
        {
            this.Errors = new List<string>();
        }

        public bool Success
        {
            get { return (this.Errors.Count == 0); }
        }

        public void AddError(string error)
        {
            this.Errors.Add(error);
        }
        
        public string CaptureTransactionId { get; set; }

        public string CaptureTransactionResult { get; set; }

        public PaymentStatus NewPaymentStatus
        {
            get
            {
                return _newPaymentStatus;
            }
            set
            {
                _newPaymentStatus = value;
            }
        }
    }
}
