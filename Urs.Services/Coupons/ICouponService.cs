﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urs.Core;
using Urs.Data.Domain.Coupons;
using Urs.Data.Domain.Users;

namespace Urs.Services.Coupons
{
    public partial interface ICouponService
    {

        #region  coupon
        void InsertCoupon(Coupon coupon);
        void UpdateCoupon(Coupon coupon);
        void DeleteCoupon(Coupon coupon);
        Coupon GetCouponById(int id);
        IPagedList<Coupon> GetCoupon(DateTime? startTime, DateTime? endTime, Decimal? miniumConsume, int PageIndex, int PageSize);
        IList<Coupon> GetUsableCoupons(int userId, decimal subTotal);
        Coupon GetUsableCoupon(int userId, decimal subTotal, int couponId);
        #endregion


        #region CouponUser

        CouponUserMapping GetCouponUserById(int id);
        IPagedList<CouponUserMapping> GetCouponsOrUser(int? userId, int? Type, int? couponId, int PageIndex, int pageSize);
        CouponUserMapping GetCouponUser(int userId, int couponId);
        IList<CouponUserMapping> GetCouponUser(int userId);
        void DeleteCouponUser(CouponUserMapping couponUser);
        void UpdateCouponUser(CouponUserMapping couponUser);
        bool InsertCouponUser(CouponUserMapping couponUser);
        #endregion


    }
}
