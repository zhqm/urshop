using System;
using System.Collections.Generic;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Orders;
using Urs.Core;

namespace Urs.Services.Orders
{
    public partial interface IShoppingCartService
    {
        void DeleteShoppingCartItem(ShoppingCartItem shoppingCartItem,
            bool ensureOnlyActiveCheckoutAttributes = false);

        int DeleteExpiredShoppingCartItems(DateTime olderThanUtc);

        IList<string> GetStandardWarnings(User user, 
            Goods goods, string selectedAttributes, int quantity);

        IList<string> GetShoppingCartItemAttributeWarnings(Goods goods, string selectedAttributes);

        IList<string> GetShoppingCartItemWarnings(User user, 
            Goods goods, string selectedAttributes,
            int quantity, bool automaticallyAddRequiredGoodssIfEnabled,
            bool getStandardWarnings = true, bool getAttributesWarnings = true,
            bool getRequiredGoodsWarnings = true);

        IList<string> GetShoppingCartWarnings(IList<ShoppingCartItem> shoppingCart);

        ShoppingCartItem FindShoppingCartItemInTheCart(IList<ShoppingCartItem> shoppingCart,
            Goods goods,
            string selectedAttributes = "");


        IList<string> AddToCart(User user, Goods goods,string selectedAttributes,
            decimal userEnteredPrice, int quantity, bool automaticallyAddRequiredGoodssIfEnabled,
            out int cartItemId, bool selected = false, bool tocheckout = false, int ptUserId = 0);
        IList<string> UpdateShoppingCartItem(User user, int shoppingCartItemId,
            int newQuantity, bool? selected = null);
        void UpdateShoppingCartItemSelected(User user, IList<ShoppingCartItem> shoppingCart, bool selected);
        void MigrateShoppingCart(User fromUser, User toUser);

    }
}
