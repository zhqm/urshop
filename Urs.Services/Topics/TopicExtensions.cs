using System;
using System.Collections.Generic;
using System.Linq;
using Urs.Data.Domain.Topics;

namespace Urs.Services.Topics
{
    public static class TopicExtensions
    {
        public static IList<TopicCategory> SortCategoriesForTree(this IList<TopicCategory> source, int parentId)
        {
            var result = new List<TopicCategory>();
 
            var temp = source.ToList().FindAll(c => c.ParentCategoryId == parentId);
            foreach (var cat in temp)
            {
                result.Add(cat);
                result.AddRange(SortCategoriesForTree(source, cat.Id));
            }
            return result;
        }
 
        public static string GetCategoryNameWithPrefix(this TopicCategory category, ITopicService topicService)
        {
            string result = string.Empty;
 
            while (category != null)
            {
                if (String.IsNullOrEmpty(result))
                    result = category.Name;
                else
                    result = "--" + result;
                category = topicService.GetTopicCategoryById(category.ParentCategoryId);
            }
            return result;
        }
 
        public static string GetCategoryBreadCrumb(this TopicCategory category, ITopicService topicService)
        {
            string result = string.Empty;
 
            while (category != null && !category.Deleted)
            {
                if (String.IsNullOrEmpty(result))
                    result = category.Name;
                else
                    result = category.Name + " >> " + result;
 
                category = topicService.GetTopicCategoryById(category.ParentCategoryId);
 
            }
            return result;
        }
 
    }
}
