using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Media;

namespace Urs.Services.Media
{
    public partial interface IPictureService
    {
        #region Methods
        string GetThumbLocalPath(Picture picture, int targetSize = 0, bool showDefaultPicture = true);
     
        string GetDefaultPictureUrl(int targetSize = 0, PictureType defaultPictureType = PictureType.Entity, bool? useSsl = null);
        int DownloadPictureUrl(string imageUrl);

        byte[] LoadPictureFromFile(int pictureId, string mimeType);

        byte[] LoadPictureBinary(Picture picture);

        string GetPictureUrl(int pictureId, int targetSize = 0, bool showDefaultPicture = true, bool? useSsl = null,
            PictureType defaultPictureType = PictureType.Entity);

        string GetPictureUrl(Picture picture, int targetSize = 0, bool showDefaultPicture = true, bool? useSsl = null,
             PictureType defaultPictureType = PictureType.Entity);

        string GetPictureLocalPath(string imageName);
        string GetPictureLocalPath(Picture picture, int targetSize = 0, bool showDefaultPicture = true);
        IList<Picture> GetPicturesByIds(int[] pictureIds);

        Picture GetPictureById(int pictureId);

        void DeletePicture(Picture picture);

        IPagedList<Picture> GetPictures(int pageIndex, int pageSize);

        IList<Picture> GetPicturesByGoodsId(int goodsId);

        IList<Picture> GetPicturesByGoodsId(int goodsId, int recordsToReturn);

        Picture InsertPicture(byte[] pictureBinary, string mimeType, string seoFilename, bool isNew, bool validateBinary = true);

        Picture UpdatePicture(int pictureId, byte[] pictureBinary, string mimeType, string seoFilename, bool isNew, bool validateBinary = true);

        Picture SetSeoFilename(int pictureId, string seoFilename);
        void MakeThumbnail(string originalImagePath, string thumbnailPath, int x1, int x2, int y1, int y2);

        byte[] GenerateQrcode(string strData, string qrEncoding = "Byte", string level = "H", int version = 8, int scale = 4);
        bool ExistsImageQrInFile(string imageName, string mimeType = "");
        void SaveImageQrInFile(string imageName, byte[] pictureBinary, string mimeType = "");
        string GetImageQrLocalPath(string imageName, string mimeType = "");
        string GetImageQrUrl(string imageName, string storeLocation = null);
        #endregion

    }
}
