using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Urs.Core;
using Urs.Core.Caching;
using Urs.Core.Data;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Configuration;
using Urs.Data.Domain.Users;

namespace Urs.Services.Users
{
    public partial class UserService : IUserService
    {
        #region Constants

        private const string CUSTOMERROLES_ALL_KEY = "Urs.userrole.all-{0}";
        private const string CUSTOMERROLES_BY_ID_KEY = "Urs.userrole.id-{0}";
        private const string CUSTOMERROLES_BY_SYSTEMNAME_KEY = "Urs.userrole.systemname-{0}";
        private const string CUSTOMERROLES_PATTERN_KEY = "Urs.userrole.";
        #endregion

        #region Fields

        private readonly IRepository<User> _userRepository;
        private readonly IRepository<UserRoleMapping> _userRoleMappingRepository;
        private readonly IRepository<UserRole> _userRoleRepository;
        private readonly ICacheManager _cacheManager;
        private readonly UserSettings _userSettings;
        private readonly IRepository<PointsHistory> _pointsHistoryRepository;
        private readonly IRepository<MessageRecord> _messageRecordRepository;

        #endregion

        #region Ctor

        public UserService(ICacheManager cacheManager,
            IRepository<User> userRepository, IRepository<UserRoleMapping> userRoleMappingRepository,
            IRepository<UserRole> userRoleRepository,
            UserSettings userSettings,
            IRepository<PointsHistory> pointsHistoryRepository,
            IRepository<MessageRecord> messageRecordRepository)
        {
            this._cacheManager = cacheManager;
            this._userRepository = userRepository;
            this._userRoleRepository = userRoleRepository;
            this._userRoleMappingRepository = userRoleMappingRepository;
            this._userSettings = userSettings;
            this._pointsHistoryRepository = pointsHistoryRepository;
            this._messageRecordRepository = messageRecordRepository;
        }

        #endregion

        #region Methods
        protected void GenerateCode(User user)
        {
            if (!string.IsNullOrWhiteSpace(user.Code))
                return;
            var code = "";
            do
            {
                code = CommonHelper.RandomCode();
            } while (GetUserByCode(code) != null && !string.IsNullOrWhiteSpace(code));

            user.Code = code;
            UpdateUser(user);
        }

        #region Users

        public virtual IPagedList<User> GetAllUsers(DateTime? registrationFrom = null,
           DateTime? registrationTo = null, int[] userRoleIds = null, string email = null, string nickname = null,
           string company = null, string phone = null, bool? active = null, bool? approved = null,
           bool loadOnlyWithShoppingCart = false, int pageIndex = 0, int pageSize = int.MaxValue,
           string provinceName = null, string cityName = null, string areaName = null)
        {
            var query = _userRepository.Table;
            if (registrationFrom.HasValue)
                query = query.Where(c => registrationFrom.Value <= c.CreateTime);
            if (registrationTo.HasValue)
                query = query.Where(c => registrationTo.Value >= c.CreateTime);
            query = query.Where(c => !c.Deleted);
            if (userRoleIds != null && userRoleIds.Length > 0)
            {
                query = query.Join(_userRoleMappingRepository.Table, x => x.Id, y => y.UserId,
                         (x, y) => new { User = x, Mapping = y })
                     .Where(z => userRoleIds.Contains(z.Mapping.UserRoleId))
                     .Select(z => z.User)
                     .Distinct();
            }
            if (!String.IsNullOrWhiteSpace(email))
                query = query.Where(c => c.Email.Contains(email));

            if (!String.IsNullOrWhiteSpace(nickname))
                query = query.Where(c => c.Nickname.Contains(nickname));

            if (!String.IsNullOrWhiteSpace(phone))
                query = query.Where(c => c.PhoneNumber.Contains(phone));
            if (active.HasValue)
                query = query.Where(c => c.Active == active.Value);

            if (approved.HasValue)
                query = query.Where(c => c.Approved == approved.Value);

            if (loadOnlyWithShoppingCart)
                query = query.Where(c => c.ShoppingCartItems.Count() > 0);

            query = query.OrderByDescending(c => c.CreateTime);

            var users = new PagedList<User>(query, pageIndex, pageSize);
            return users;
        }

        public User GetUsersByUsernameOrPhoneOrEmail(string usernameOrPhoneOrEmail)
        {
            if (string.IsNullOrWhiteSpace(usernameOrPhoneOrEmail))
                return null;

            var query = from u in _userRepository.Table
                        where
                            u.Email == usernameOrPhoneOrEmail ||
                            u.PhoneNumber == usernameOrPhoneOrEmail && !u.Deleted
                        select u;
            return query.FirstOrDefault();
        }

        public virtual void DeleteUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (user.IsSystemAccount)
                throw new UrsException(string.Format("System user account ({0}) could not be deleted", user.SystemName));

            user.Deleted = true;
            UpdateUser(user);
        }

        public virtual User GetUserById(int userId)
        {
            if (userId == 0)
                return null;

            var user = _userRepository.GetById(userId);
            return user;
        }

        public virtual IList<User> GetUsersByIds(int[] userIds)
        {
            if (userIds == null || userIds.Length == 0)
                return new List<User>();

            var query = from c in _userRepository.Table
                        where userIds.Contains(c.Id)
                        select c;
            var users = query.ToList();
            var sortedUsers = new List<User>();
            foreach (int id in userIds)
            {
                var user = users.Find(x => x.Id == id);
                if (user != null)
                    sortedUsers.Add(user);
            }
            return sortedUsers;
        }

        public virtual User GetUserByGuid(Guid userGuid)
        {
            if (userGuid == Guid.Empty)
                return null;

            var query = from c in _userRepository.Table
                        where c.UserGuid == userGuid
                        orderby c.Id
                        select c;
            var user = query.FirstOrDefault();
            return user;
        }

        public virtual User GetUserByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;

            var query = from c in _userRepository.Table
                        orderby c.Id
                        where c.Email == email
                        select c;
            var user = query.FirstOrDefault();
            return user;
        }

        public virtual User GetUserByPhone(string phone)
        {
            if (string.IsNullOrWhiteSpace(phone))
                return null;

            var query = from c in _userRepository.Table
                        orderby c.Id
                        where c.PhoneNumber == phone
                        select c;
            var user = query.FirstOrDefault();
            return user;
        }
        public virtual User GetUserByCode(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
                return null;

            var query = from c in _userRepository.Table
                        orderby c.Id
                        where c.Code == code
                        select c;
            var user = query.FirstOrDefault();
            return user;
        }
        public virtual User InsertGuestUser()
        {

            var user = new User()
            {
                UserGuid = Guid.NewGuid(),
                Active = true,
                CreateTime = DateTime.Now,
                LastActivityTime = DateTime.Now,
            };

            var guestRole = GetUserRoleBySystemName(SystemRoleNames.Guests);
            if (guestRole == null)
                throw new UrsException("'Guests' role could not be loaded");
            _userRepository.Insert(user);
            user.UserRoleMappings.Add(new UserRoleMapping { UserRole = guestRole });

            GenerateCode(user);

            return user;
        }

        public virtual void InsertUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");


            _userRepository.Insert(user);
            GenerateCode(user);
        }

        public virtual void UpdateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            _userRepository.Update(user);
        }

        protected virtual void LoopDeleteGuestUsers(IQueryable<User> query, int pageIndex, int pageSize, ref int numberOfDeletedUsers)
        {
            var users = new PagedList<User>(query, pageIndex, pageSize);

            foreach (var c in users)
            {
                try
                {
                    _userRepository.Delete(c);
                    numberOfDeletedUsers++;
                }
                catch (Exception exc)
                {
                    Debug.WriteLine(exc);
                }
            }

            if (users.TotalPages - 1 > pageIndex)
                LoopDeleteGuestUsers(query, pageIndex + 1, pageSize, ref numberOfDeletedUsers);
        }

        public virtual int DeleteGuestUsers(DateTime? createdToUtc)
        {
            var guestRole = GetUserRoleBySystemName(SystemRoleNames.Guests);
            if (guestRole == null)
                return 0;

            var query = _userRepository.Table;
            if (createdToUtc.HasValue)
                query = query.Where(c => createdToUtc.Value >= c.CreateTime);

            query = from c in query
                    from cr in c.UserRoleMappings
                    where guestRole.Id == cr.UserRoleId
                    orderby c.CreateTime descending
                    select c;

            query = query.Where(c => c.ShoppingCartItems.Count() == 0);
            query = query.Where(c => !c.IsSystemAccount);

            int numberOfDeletedUsers = 0;
            LoopDeleteGuestUsers(query, 0, 10, ref numberOfDeletedUsers);

            return numberOfDeletedUsers;
        }

        #endregion

        #region User roles

        public virtual void DeleteUserRole(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("userRole");

            if (userRole.IsSystemRole)
                throw new UrsException("System role could not be deleted");

            _userRoleRepository.Delete(userRole);

            _cacheManager.RemoveByPattern(CUSTOMERROLES_PATTERN_KEY);
        }

        public virtual UserRole GetUserRoleById(int userRoleId)
        {
            if (userRoleId == 0)
                return null;

            var userRole = _userRoleRepository.GetById(userRoleId);
            return userRole;
        }

        public virtual UserRole GetUserRoleBySystemName(string systemName)
        {
            if (String.IsNullOrWhiteSpace(systemName))
                return null;

            string key = string.Format(CUSTOMERROLES_BY_SYSTEMNAME_KEY, systemName);
            return _cacheManager.Get(key, () =>
            {
                var query = from cr in _userRoleRepository.Table
                            orderby cr.Id
                            where cr.SystemName == systemName
                            select cr;
                var userRole = query.FirstOrDefault();
                return userRole;
            });
        }

        public virtual IList<UserRole> GetAllUserRoles(bool showHidden = false)
        {
            string key = string.Format(CUSTOMERROLES_ALL_KEY, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = from cr in _userRoleRepository.Table
                            orderby cr.Name
                            where (showHidden || cr.Active)
                            select cr;
                var userRoles = query.ToList();
                return userRoles;
            });
        }

        public virtual void InsertUserRole(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("userRole");

            _userRoleRepository.Insert(userRole);

            _cacheManager.RemoveByPattern(CUSTOMERROLES_PATTERN_KEY);
        }

        public virtual void UpdateUserRole(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("userRole");

            _userRoleRepository.Update(userRole);

            _cacheManager.RemoveByPattern(CUSTOMERROLES_PATTERN_KEY);
        }

        #endregion

        #region RewardPointsHistory

        public virtual IPagedList<PointsHistory> GetPointsHistory(int userId, DateTime? startTime, DateTime? endTime, int pageIndex, int pageSize)
        {
            var query = _pointsHistoryRepository.Table.Where(o => o.UserId == userId);

            if (startTime.HasValue)
                query = query.Where(o => startTime.Value <= o.CreateTime);
            if (endTime.HasValue)
                query = query.Where(o => endTime.Value >= o.CreateTime);

            query = query.OrderByDescending(c => c.CreateTime);
            var points = new PagedList<PointsHistory>(query, pageIndex, pageSize);
            return points;
        }
        #endregion


        #region 邀请人
        public IPagedList<User> GetInvitations(int affiliateId, int pageIndex, int pageSize)
        {
            var registerRole = GetUserRoleBySystemName(SystemRoleNames.Registered);
            if (registerRole == null)
                throw new UrsException("'registerRole' role could not be loaded");
            var query = _userRepository.Table;

            if (affiliateId > 0)
                query = query.Where(q => q.AffiliateId.Value == affiliateId);

            query = from c in query
                    from cr in c.UserRoleMappings
                    where registerRole.Id == cr.UserRoleId
                    orderby c.CreateTime descending
                    select c;

            return new PagedList<User>(query, pageIndex, pageSize);
        }

        public IList<User> GetInvitation(int userId, UserRoleType type)
        {
            if (userId <= 0)
                return null;
            var query = _userRepository.Table;
            query = query.Where(q => q.Active && q.Approved && !q.Deleted);

            var a = from q in query
                    where q.AffiliateId == userId
                    select q;

            return a.ToList();
        }

        public int GetInvitedCount(int userId)
        {
            if (userId <= 0)
                return 0;

            var query = _userRepository.Table;
            query = query.Where(q => q.Active && q.Approved && !q.Deleted);

            query = from q in query
                    where q.AffiliateId == userId
                    select q;

            return query.Count();
        }
        #endregion

        #region 邀请码短信记录
        public void InsertMessageRecord(MessageRecord messageRecord)
        {
            if (messageRecord == null)
                throw new ArgumentNullException("invited msg");
            messageRecord.SendTime = DateTime.Now;
            messageRecord.Mark = "生成信息";
            _messageRecordRepository.Insert(messageRecord);
        }
        public void UpdateMessageRecord(MessageRecord messageRecord)
        {
            if (messageRecord == null)
                throw new ArgumentNullException("invited msg");

            _messageRecordRepository.Update(messageRecord);
        }
        public void DeleteMessageRecord(MessageRecord messageRecord)
        {
            if (messageRecord == null)
                throw new ArgumentNullException("invited msg");

            _messageRecordRepository.Delete(messageRecord);
        }

        public MessageRecord GetMessageRecord(int userId, string mark = null, DateTime? startTime = null, DateTime? endTime = null)
        {
            var query = _messageRecordRepository.Table.Where(q => q.UserId == userId);

            if (!string.IsNullOrEmpty(mark))
                query = query.Where(q => q.Mark == mark);

            if (startTime.HasValue)
                query = query.Where(q => q.SendTime >= startTime.Value);

            if (endTime.HasValue)
                query = query.Where(q => q.SendTime <= endTime.Value);

            return query.FirstOrDefault();
        }
        public MessageRecord GetMessageRecord(string phone, DateTime startTime)
        {
            var query = _messageRecordRepository.Table.Where(q => q.Phone == phone);
            query = query.Where(q => q.SendTime >= startTime);
            return query.FirstOrDefault();
        }
        public MessageRecord GetMessageRecordById(int messagerecordId)
        {
            if (messagerecordId == 0)
                return null;

            var record = _messageRecordRepository.GetById(messagerecordId);
            return record;


        }
        public IList<MessageRecord> GetMessageRecordByUserId(int userId)
        {
            if (userId == 0)
                return null;

            var query = _messageRecordRepository.Table;
            query = query.Where(q => q.UserId == userId);

            return query.ToList();

        }

        public IPagedList<MessageRecord> GetMessageRecordByUserId(int userId, int pageIndex, int pageSize)
        {
            if (userId == 0)
                return null;

            var query = _messageRecordRepository.Table;
            query = query.Where(q => q.UserId == userId);

            var records = new PagedList<MessageRecord>(query, pageIndex, pageSize);

            return records;
        }

        public IPagedList<MessageRecord> GetMessageRecords(int? userId, string phone, DateTime? startTime, DateTime? endtime, int pageIndex, int pageSize)
        {
            var query = _messageRecordRepository.Table;
            if (userId.HasValue)
                query = query.Where(q => q.UserId == userId);
            if (string.IsNullOrWhiteSpace(phone))
                query = query.Where(q => q.Phone == phone);
            if (startTime.HasValue)
                query = query.Where(q => q.SendTime >= startTime);
            if (endtime.HasValue)
                query = query.Where(q => q.SendTime <= endtime);

            var record = new PagedList<MessageRecord>(query, pageIndex, pageSize);
            return record;
        }
        #endregion

        #endregion

    }
}