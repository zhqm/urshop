﻿using Urs.Framework.Mvc;

namespace Urs.Framework.Models
{
    public partial class BaseEntityModel : BaseModel
    {
        public virtual int Id { get; set; }
    }
}
