﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Urs.Framework.Mvc.Routes;

namespace Urs.Plugin.Payments.WeixinOpen.Infrastructure
{
    public partial class RouteProvider : IRouteProvider
    {
        /// <summary>
        /// Register routes
        /// </summary>
        /// <param name="routeBuilder">Route builder</param>
        public void RegisterRoutes(IRouteBuilder routes)
        {
            routes.MapRoute("Plugin.Payments.WeixinOpen.Configure",
               "Plugins/PaymentWeixinOpen/Configure",
               new { controller = "PaymentWeixinOpen", action = "Configure" });

            routes.MapRoute("Plugin.Payments.WeixinOpen.PaymentInfo",
                 "Plugins/PaymentWeixinOpen/PaymentInfo",
                 new { controller = "PaymentWeixinOpen", action = "PaymentInfo" });

            routes.MapRoute("Plugin.Payments.WeixinOpen.Paying",
                 "Plugins/PaymentWeixinOpen/Paying",
                 new { controller = "PaymentWeixinOpen", action = "Paying" }
            );

            routes.MapRoute("Plugin.Payments.WeixinOpen.PayingApi",
                 "Plugins/PaymentWeixinOpen/PayingApi",
                 new { controller = "PaymentWeixinOpen", action = "PayingApi" }
            );

            //Notify
            routes.MapRoute("Plugin.Payments.WeixinOpen.Notify",
                 "Plugins/PaymentWeixinOpen/Notify",
                 new { controller = "PaymentWeixinOpen", action = "Notify" }
            );

            //Notify
            routes.MapRoute("Plugin.Payments.WeixinOpen.Return",
                 "Plugins/PaymentWeixinOpen/Return",
                 new { controller = "PaymentWeixinOpen", action = "Return" }
            );
        }

        /// <summary>
        /// Gets a priority of route provider
        /// </summary>
        public int Priority
        {
            get { return 0; }
        }
    }
}