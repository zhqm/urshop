//Contributor:  Nicholas Mayne


using Urs.Services.Authentication.External;

namespace Urs.Plugin.ExternalAuth.WeixinOpen.Core
{
    public interface IWeixinOpenProviderAuthorizer : IExternalProviderAuthorizer
    {
    }
}