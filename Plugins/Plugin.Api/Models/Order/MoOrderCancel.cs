﻿namespace Plugin.Api.Models.Order
{
    /// <summary>
    /// 订单详情
    /// </summary>
    public partial class MoOrderCancel 
    {
        /// <summary>
        /// 用户Guid
        /// </summary>
        public string UserGuid { get; set; }
        /// <summary>
        /// 订单Guid
        /// </summary>
        public string OrderGuid { get; set; }
    }
}