﻿using System.Collections.Generic;
using Plugin.Api.Models.Common;

namespace Plugin.Api.Models.Catalog
{
    /// <summary>
    /// 品牌列表
    /// </summary>
    public partial class MoBrandList
    {
        public MoBrandList()
        {
            Items = new List<MoSimpleBrand>();
            Paging = new MoPaging();
        }
        public IList<MoSimpleBrand> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }

    }
}