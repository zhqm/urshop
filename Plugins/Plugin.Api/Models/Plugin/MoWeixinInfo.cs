﻿using System.Collections.Generic;

namespace Plugin.Api.Models.Plugin
{
    /// <summary>
    /// 微信对象
    /// </summary>
    public class MoWeixinInfo
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string nickname { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string gender { get; set; }
        /// <summary>
        /// 头像Url
        /// </summary>
        public string avatarurl { get; set; }
    }
}