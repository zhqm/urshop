﻿using System.Collections.Generic;

namespace Plugin.Api.Models.Plugin
{
    /// <summary>
    /// 支付对象
    /// </summary>
    public class MoPayment
    {
        public MoPayment()
        {
            KeyValue = new Dictionary<string, string>();
        }
        /// <summary>
        /// 订单编号
        /// </summary>
        public int OrderId { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        public string OrderPrice { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 支付网址（支付宝才有）
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 支付方法
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// 传递的支付表单
        /// </summary>
        public Dictionary<string, string> KeyValue { get; set; }
    }
}