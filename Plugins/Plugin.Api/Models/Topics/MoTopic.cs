﻿using Urs.Framework.Mvc;

namespace Plugin.Api.Models.Topics
{
    /// <summary>
    /// 单页新
    /// </summary>
    public partial class MoTopic
    {
        /// <summary>
        /// Id编码
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 系统关键词
        /// </summary>
        public string SystemName { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Short { get; set; }
        /// <summary>
        /// 内容（Html)
        /// </summary>
        public string Body { get; set; }
        /// <summary>
        /// 是否发布
        /// </summary>
        public bool Published { get; set; }
    }
}
