﻿using System;
using System.Collections.Generic;
using Plugin.Api.Models.Media;

namespace Plugin.Api.Models.Common
{
    public partial class MoBannerZone
    {
        public MoBannerZone()
        {
            Items = new List<MoBannerItem>();
        }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类样式名称
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// 区域名称
        /// </summary>
        public string ZoneName { get; set; }
        /// <summary>
        /// 模板名称
        /// </summary>
        public string TemplateName { get; set; }

        public IList<MoBannerItem> Items { get; set; }


        public partial class MoBannerItem
        {
            public string Title { get; set; }
            public string SubTitle { get; set; }
            public string Url { get; set; }
            public MoPicture Picture { get; set; }
        }
    }
}